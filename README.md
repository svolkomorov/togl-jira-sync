# Toggl->jira Worklog sync tool

The tool exports all Toggl.com worklog entries for specified dates and sends them to Jira Tempo.

### Build instructions:

1. Install node v18+ and npm
1. `npm ci && npm run build && npm run package` to build for Linux or `npm ci && npm run build && npm run package-macos` to build for macOS

> ### :exclamation: Disclaimer :exclamation:
> 
> The script will interactively ask you for all the credentials and configuration options it needs to work but it will also offer you an option to save its configuration to the `.env` file. Everything will be saved as PLAIN TEXT, and therefore it's NOT SAFE/SECURE. Use at your own risk.
> 
> You can also do it manually (still not safe):
> 1. `cp env.dist .env` and modify `.env`.
> 1. Provide options via CLI environment variables, e.g. `JIRA_LOGIN=user JIRA_PASSWORD=12345 ./build/sync`.

### Usage instructions:
1. `./build/sync` or `./build/sync --week` to process the last 7 days
1. `./build/sync --today` to process the current day
1. `./build/sync <YYYY-MM-DD>` to process a single day
1. `./build/sync <YYYY-MM-DD> <YYYY-MM-DD>` to process the specified range

### Known issues

1. Build for macos fails on linux:
    ```
    > Warning Failed to make bytecode node18-arm64 for file /snapshot/build/sync.js
    > Warning Unable to sign the macOS executable
      Due to the mandatory code signing requirement, before the
      executable is distributed to end users, it must be signed.
      Otherwise, it will be immediately killed by kernel on launch.
      An ad-hoc signature is sufficient.
      To do that, run pkg on a Mac, or transfer the executable to a Mac
      and run "codesign --sign - <executable>", or (if you use Linux)
      install "ldid" utility to PATH and then run pkg again
    ```
1. `./build/sync` binary size is gigantic (40 mb)
