import select from '@inquirer/select';

import { getConfigFromArgs, getEnv, md5 } from './utils';
import { getTogglEntries, getTogglWorkspaces } from './api/toggl';
import { deleteJiraWorklog, getJiraWorklog, postJiraWorklog } from './api/jira';
import { JiraRequestEntry, JiraWorklogEntry } from './types';

(async () => {
    const { dates: { toggl, jira } } = getConfigFromArgs();

    type TaskHash = string;
    function getHash({
        comment,
        started,
        timeSpentSeconds,
        issue: { key },
    }: Pick<JiraWorklogEntry,
        | 'comment'
        | 'started'
        | 'timeSpentSeconds'
        | 'issue'
    >): TaskHash {
        return md5([
            comment,
            started,
            timeSpentSeconds,
            key,
        ].join(':'));
    }

    const jiraWorklogEntries = await getJiraWorklog({ start: jira.start, end: jira.end });
    const jiraWorklogEntriesMap: Record<TaskHash, JiraWorklogEntry> = jiraWorklogEntries.reduce((acc, entry) => {
        acc[getHash(entry)] = entry;

        return acc;
    }, {});

    const taskIdRegExp = new RegExp(await getEnv('TOGGL_DESCRIPTION_TASK_REGEXP'));

    const workspaceId = Number(await getEnv('TOGGL_WORKSPACE_ID', async () => {
        const togglWorkspaces = await getTogglWorkspaces();

        if (togglWorkspaces.length === 1) {
            return String(togglWorkspaces[0].id);
        }

        return await select({
            message: 'Please select Toggl workspace',
            choices: togglWorkspaces.map(({ name, id  }) => ({
                name,
                value: String(id),
                description: `${name} (id=${id})`,
            }))
        })
    }));

    console.log("Creating new Jira worklog entries...");
    const processedHashes = [];
    await (Promise.all(
        (await getTogglEntries({ start: toggl.start, end: toggl.end }))
            .filter(({ workspace_id }) => workspace_id === workspaceId)
            .map((entry) => {
                const originTaskId = entry.description.match(taskIdRegExp)?.[0];

                if (originTaskId && entry.duration > 0) {
                    const newEntry: JiraRequestEntry = {
                        comment: entry.description,
                        started: (new Date(entry.start)).toISOString().replace('Z', '').replace('T', ' '),
                        timeSpentSeconds: entry.duration,
                        originTaskId,
                    };

                    const hash = getHash({
                        comment: newEntry.comment,
                        started: newEntry.started,
                        timeSpentSeconds: newEntry.timeSpentSeconds,
                        issue: {
                            key: newEntry.originTaskId,
                        },
                    });
                    processedHashes.push(hash);

                    if (!jiraWorklogEntriesMap[hash]) {
                        return postJiraWorklog(newEntry);
                    }
                }

                return Promise.resolve();
            })
    ));

    const hashesToRemove = Object.keys(jiraWorklogEntriesMap).filter((hash) => !processedHashes.includes(hash));

    if (hashesToRemove.length) {
        console.log("Deleting old Jira worklog entries...");

        await (Promise.all(hashesToRemove.map((hash) => {
            const { tempoWorklogId } = jiraWorklogEntriesMap[hash];

            return deleteJiraWorklog(String(tempoWorklogId));
        })));

        console.log("Done!");
    }

    console.log("All done!");
})();
