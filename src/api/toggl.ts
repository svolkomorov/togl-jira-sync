import { formatDate, getEnv, fetchJson } from '../utils';
import type { DateRangeRequest, TogglEntry, TogglWorkspace } from '../types';

async function getTogglData(url: string, query = {}): Promise<any> {
    const result = (await fetchJson(`https://api.track.toggl.com/${url}?` + (new URLSearchParams(query)).toString(), {
        method: 'GET',
        login: await getEnv('TOGGL_API_TOKEN'),
        password: 'api_token',
    }));

    if (typeof result === 'string') {
        throw new Error(result);
    }

    return result;
}

export async function getTogglEntries({ start, end }: DateRangeRequest): Promise<TogglEntry[]> {
    return getTogglData('api/v9/me/time_entries', {
        start_date: formatDate(start),
        end_date: formatDate(end),
    });
}

export async function getTogglWorkspaces(): Promise<TogglWorkspace[]> {
    return getTogglData('api/v9/me/workspaces');
}
