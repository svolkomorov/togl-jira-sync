import type { JiraRequestEntry, DateRangeRequest, JiraWorklogEntry } from '../types';
import { fetchJson, formatDate, getEnv } from '../utils';

export async function postJiraWorklog(data: JiraRequestEntry): Promise<void> {
    await jiraRequest('/rest/tempo-timesheets/4/worklogs/', 'POST', {
        ...data,
        worker: await getJiraWorkerKey(),
    });
}

export async function getJiraWorklog({ start, end }: DateRangeRequest): Promise<JiraWorklogEntry[]> {
    return (await jiraRequest('/rest/tempo-timesheets/4/worklogs/search', 'POST', {
        from: formatDate(start),
        to: formatDate(end),
        worker: [await getJiraWorkerKey()],
    }));
}

export async function deleteJiraWorklog(worklogId: string): Promise<void> {
    await jiraRequest(`/rest/tempo-timesheets/4/worklogs/${worklogId}`, 'DELETE');
}

async function jiraRequest (url: string, method: 'GET'|'POST'|'DELETE', data?: Record<string, any>): Promise<any> {
    return (await fetchJson(
        (await getEnv('JIRA_BASE_URL')) + url,
        {
            method,
            login: await getEnv('JIRA_LOGIN'),
            password: await getEnv('JIRA_PASSWORD'),
            data,
        }
    ));
}

let workerKey = '';
async function getJiraWorkerKey(): Promise<string> {
    if (workerKey) {
        return workerKey;
    }

    return workerKey = (await jiraRequest(
        '/rest/api/2/user?username=' + await getEnv('JIRA_LOGIN'),
        'GET'
    )).key;
}
