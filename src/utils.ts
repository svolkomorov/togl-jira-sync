import dotenv from 'dotenv';
import commandLineArgs from 'command-line-args';
import { writeFileSync } from 'node:fs';
import https from 'node:https';
import { parse } from 'node:url';
import crypto from 'node:crypto';

import input from '@inquirer/input';
import password from '@inquirer/password';
import confirm from '@inquirer/confirm';

import type { EnvKeys, Env, Options, Config } from './types';

dotenv.config();

export function formatDate(date: Date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');

    return `${year}-${month}-${day}`;
}

export function basicAuthorization(login: string, password: string) {
    return `Basic ${Buffer.from(login + ':' + password).toString('base64')}`;
}

const envDefaults: Env = {
    JIRA_LOGIN: '',
    JIRA_PASSWORD: '',
    TOGGL_API_TOKEN: '',
    TOGGL_WORKSPACE_ID: '',
    JIRA_BASE_URL: '',
    TOGGL_DESCRIPTION_TASK_REGEXP: '',
    CONFIG_SAVE_AGREEMENT: '0',
};

async function syncEnv (): Promise<void> {
    if (!saveAgreement) {
        return;
    }

    const env: Env = {
        JIRA_LOGIN: newEnv.get('JIRA_LOGIN') || process.env['JIRA_LOGIN'] || envDefaults['JIRA_LOGIN'],
        JIRA_PASSWORD: newEnv.get('JIRA_PASSWORD') || process.env['JIRA_PASSWORD'] || envDefaults['JIRA_PASSWORD'],
        TOGGL_API_TOKEN: newEnv.get('TOGGL_API_TOKEN') || process.env['TOGGL_API_TOKEN'] || envDefaults['TOGGL_API_TOKEN'],
        TOGGL_WORKSPACE_ID: newEnv.get('TOGGL_WORKSPACE_ID') || process.env['TOGGL_WORKSPACE_ID'] || envDefaults['TOGGL_WORKSPACE_ID'],
        JIRA_BASE_URL: newEnv.get('JIRA_BASE_URL') || process.env['JIRA_BASE_URL'] || '',
        TOGGL_DESCRIPTION_TASK_REGEXP: newEnv.get('TOGGL_DESCRIPTION_TASK_REGEXP') || process.env['TOGGL_DESCRIPTION_TASK_REGEXP'] || envDefaults['TOGGL_DESCRIPTION_TASK_REGEXP'],
        CONFIG_SAVE_AGREEMENT: saveAgreement ? '1' : '0',
    };

    writeFileSync('./.env', Object.entries(env).map(([key, value]) => `${key}=${value}`).join('\n'));
}

const newEnv = new Map<string, string>();
let saveAgreement: boolean|undefined;
export async function getEnv(key: EnvKeys, getter?: () => Promise<string>): Promise<string> {
    const defaultSaveAgreement = (process.env as Env)['CONFIG_SAVE_AGREEMENT'];
    if (defaultSaveAgreement !== undefined) {
        saveAgreement = defaultSaveAgreement === '1';
    }

    if (saveAgreement === undefined) {
        saveAgreement = await confirm({
            message: 'Do you want to save your configuration to an .env file (NOT SECURE)?',
            default: false,
        });
        await syncEnv();
    }

    let value: string|undefined = (process.env as Env)[key] || newEnv.get(key);

    if (!value) {
        value = getter
            ? await getter()
            : (
                ['JIRA_PASSWORD', 'TOGGL_API_TOKEN'].includes(key)
                    ? await password({
                        message: `Please input value for '${key}'`,
                    })
                    : await input({
                        message: `Please input value for '${key}'`,
                        default: envDefaults[key],
                    })
            ) as string;
        newEnv.set(key, value);
        await syncEnv();
    }

    return value;
}

type JsonRequest = {
    method: 'POST' | 'GET' | 'DELETE',
    login: string,
    password: string,
    data?: Record<string, string>,
};

export function fetchJson(url: string, {
    method,
    data,
    login,
    password,
}: JsonRequest): Promise<any> {
    const { pathname, search, hostname } = parse(url);

    return new Promise((resolve, reject) => {
        const postData = data ? JSON.stringify(data) : null;
        const req = https.request({
            method,
            hostname,
            path: pathname + (search || ''),
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': postData ? Buffer.byteLength(postData) : 0,
                'Authorization': basicAuthorization(login, password),
            },
        }, (res) => {
            let data: any[] = [];
            res.on('data', chunk => {
                data.push(chunk);
            });
            res.on('end', () => {
                try {
                    let stringData: string = Buffer.concat(data).toString();
                    resolve(stringData ? JSON.parse(stringData) : null);
                } catch (err: any) {
                    reject(err);
                }
            });
        }).on('error', (err) => {
            reject(err);
        });

        if (postData) {
            req.write(postData);
        }

        req.end();
    });
}

export function getConfigFromArgs(): Config {
    const optionDefinitions: commandLineArgs.OptionDefinition[] = [
        { name: 'today', alias: 'd', type: Boolean, defaultValue: false },
        { name: 'week', alias: 'w', type: Boolean, defaultValue: false },
        { name: 'dates', type: String, multiple: true, defaultOption: true },
    ];

    const args = commandLineArgs(optionDefinitions, { partial: true }) as Options;

    let startToggl: Date;
    let endToggl: Date;
    let startJira: Date;
    let endJira: Date;

    if (
        (args.today || args.week) && args.dates?.length
        || (args.today && args.week)
    ) {
        console.error("Can't use --today and --week arguments together or combined with dates");
        process.exit(1);
    }

    if (args.today) {
        startToggl = new Date();
        endToggl = new Date(startToggl.getTime() + (1 * 24 * 60 * 60 * 1000));
        console.log(`Processing today, ${formatDate(startToggl)}`);
    } else if (!args.dates?.length || args.week) {
        endToggl = new Date();
        startToggl = new Date(endToggl.getTime() - (7 * 24 * 60 * 60 * 1000));
        console.log(`Processing last 7 days, ${formatDate(startToggl)}...${formatDate(endToggl)}`);
    } else if (args.dates?.length === 1) {
        startToggl = new Date(args.dates[0]);
        endToggl = new Date(startToggl.getTime() + (1 * 24 * 60 * 60 * 1000));
        console.log(`Processing ${formatDate(startToggl)}`);
    } else {
        startToggl = new Date(args.dates[0]);
        endToggl = new Date(args.dates[1]);
        console.log(`Processing ${formatDate(startToggl)}...${formatDate(endToggl)}`);
    }
    startJira = startToggl;
    endJira = new Date(endToggl.getTime() - (1 * 24 * 60 * 60 * 1000));

    return {
        dates: {
            toggl: {
                start: startToggl,
                end: endToggl,
            },
            jira: {
                start: startJira,
                end: endJira,
            },
        },
    };
}

export function md5(str: string): string {
    return crypto.createHash('md5').update(str).digest('hex');
}