type DateLikeString = string;
export type TogglEntry = {
    id: number,
    workspace_id: number,
    project_id: number,
    task_id: number,
    billable: boolean,
    start: DateLikeString,
    stop: DateLikeString,
    duration: number,
    description: string,
    tags: string[],
    tag_ids: number[],
    duronly: boolean,
    at: DateLikeString,
    server_deleted_at: DateLikeString|null,
    user_id: number,
    uid: number,
    wid: number,
    pid: number
};

export type TogglWorkspace = {
    id: number,
    name: string,
};

export type JiraRequestEntry = {
    comment: string,
    started: DateLikeString,
    timeSpentSeconds: number,
    originTaskId: string
};

export type JiraWorklogEntry = {
    comment: string,
    started: DateLikeString,
    timeSpentSeconds: number,
    issue: {
        key: string,
    }
}

export type DateRangeRequest = {
    start: Date,
    end: Date,
};

type Dates = {
    toggl: { start: Date, end: Date },
    jira: { start: Date, end: Date },
};

export type Config = {
    dates: Dates,
}

export type Options = {
    week: boolean,
    today: boolean,
    dates?: string[],
};

export type Env = {
    JIRA_LOGIN?: string;
    JIRA_PASSWORD?: string;
    TOGGL_API_TOKEN?: string;
    TOGGL_WORKSPACE_ID?: string;
    JIRA_BASE_URL?: string;
    TOGGL_DESCRIPTION_TASK_REGEXP?: string;
    CONFIG_SAVE_AGREEMENT?: '1'|'0';
};

export type EnvKeys = keyof Env;
